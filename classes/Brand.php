<?php
namespace Classes;

define('BRAND_DATA_DIR', 'data' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR);

class Brand
{
    private $json = null;
    private $name = null;
    private $quality = null;

    public function __construct($brand_json = null)
    {
        if($brand_json === null)
            throw new Exception('Nincs megadva termék.');

        $this->json = $brand_json . '.json';
        $f = fopen(BRAND_DATA_DIR . $this->json, "r") or die('Nincs ilyen azonosítójú márka a nyilvántartásban');
        $data = json_decode(fread($f, filesize(BRAND_DATA_DIR . $this->json)));
        fclose($f);

        $this->name = $data->name;
        $this->quality = $data->quality;
    }

    public function toString()
    {
        $ret = 'MÁRKA: ' . $this->name . '(minőség: ' . $this->quality . ')';
        return $ret;
    }
}