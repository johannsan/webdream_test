<?php
namespace Classes;

define('PRODUCT_DATA_DIR', 'data' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR);

class Product
{
    private $json = null;
    private $id = null;
    private $name = null;
    private $price = null;
    private $brand = null;

    public function __construct($product_id = null)
    {
        if($product_id === null)
            throw new Exception('Nincs megadva termék.');

        $this->json = $product_id . '.json';
        $f = fopen(PRODUCT_DATA_DIR . $this->json, "r") or die('Nincs ilyen azonosítójú termék a nyilvántartásban');
        $data = json_decode(fread($f, filesize(PRODUCT_DATA_DIR . $this->json)));
        fclose($f);

        $this->id = $product_id;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->brand = new Brand($data->brand);
    }

    public function toString()
    {
        $ret = 'TERMÉK: ' . $this->name . '(azon:' . $this->id . ')' . ', ÁR: ' . $this->price . ', ' . $this->brand->toString() . PHP_EOL;
        return $ret;
    }
}