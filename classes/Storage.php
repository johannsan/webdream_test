<?php
namespace Classes;

define('STORAGE_DATA_DIR', 'data' . DIRECTORY_SEPARATOR . 'storages' . DIRECTORY_SEPARATOR);

class Storage
{
    private $json = null;
    private $id = null;
    private $name = null;
    private $address = null;
    private $capacity = null;
    private $products = [];

    public function __construct($storage_json = null)
    {
        if($storage_json === null)
            throw new Exception('Nincs megadva raktárállomány.');

        $this->json = $storage_json;
        $f = fopen(STORAGE_DATA_DIR . $this->json, "r");
        $data = json_decode(fread($f, filesize(STORAGE_DATA_DIR . $this->json)));
        fclose($f);

        $this->name = $data->name;
        $this->address = $data->address;
        $this->capacity = $data->capacity;
        foreach($data->products->ids as $prod_id)
        {
            $this->products[] = new Product($prod_id);
        }
    }

    public function toString()
    {
        $ret = 'TELEPHELY : ' . $this->name . ', cím: ' . $this->address . ', kapacitás: ' . $this->capacity . '(szabad helyek: ' . ($this->capacity - count($this->products)) . ')' . PHP_EOL;
        foreach($this->products as $product)
        {
            $ret .= $product->toString();
        } 
        return $ret;
    }
}