<?php
require __DIR__.'/vendor/autoload.php';
 
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Classes\Storage;

define('STORAGES_DIR', 'data' . DIRECTORY_SEPARATOR . 'storages');
define('PRODUCTS_DIR', 'data' . DIRECTORY_SEPARATOR . 'products');
define('BRANDS_DIR', 'data' . DIRECTORY_SEPARATOR . 'brands');
 
$app = new Application('Vasbányai János Webdream teszt', '1.0.0');

$app->register('storage')
    ->addArgument('action', InputArgument::OPTIONAL, 'What to do with storage')
    ->addOption('params', null, InputOption::VALUE_REQUIRED, 'asdf')
    ->setCode(function (InputInterface $input, OutputInterface $output){
        $action = $input->getArgument('action');
        $params = $input->getOption('params');
            switch ($action)
            {
                case 'list':
                    foreach (new DirectoryIterator(STORAGES_DIR) as $fileInfo)
                    {
                        if($fileInfo->getExtension() !== 'json') continue;
                        $output->writeln('<info>' . (new Storage($fileInfo->getFilename()))->toString() . '</info>');
                    }
                    return $output;
                case 'new':
                    if(!empty($params))
                    {
                        //NOT IMPLEMENTED
                    } else {
                        $output->writeln('<info>NOT IMPLEMENTED</info>');
                    }
                default:
                    return $output->writeln("<info>Használja az alábbi parancsok valamelyikét: [list] [new]</info>");
            }
    });

$app->run();